console.log('content.js is ready!');

$(document).ready(function() {
	console.log('content document ready');
	
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		console.log(sender.tab ? "from a content script:" + sender.tab.url : 'message from the extension', request.command);
		
	});
	
	window.setInterval(function() {
		console.log('interval');
		
		var bonus_points_selector = ".claimable-bonus__icon";
		
		if ($(bonus_points_selector).length > 0) {
			$(bonus_points_selector)[0].click();
		}
	}, 60000);
});
